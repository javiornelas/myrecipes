const query = require("../db/query");
const db = require("../db/connect");

const database_name = 'my_simple_cookbook';

exports.create = async (profile_id, recipe) => {
  try {
    const {
      name,
      cook_time,
      prep_time,
      servings,
      calories,
      is_public,
      img_link,
    } = recipe;
    let { ingredients, steps } = recipe;

    ingredients = JSON.stringify(ingredients);
    steps = JSON.stringify(steps);

    let sql =
      `INSERT INTO ${database_name}.recipe` +
      ` (profile_id, name, cook_time, prep_time, calories,` +
      ` servings, ingredients, steps, is_public, img_link) VALUES` +
      ` (${profile_id},"${name}",${cook_time},${prep_time},` +
      ` ${calories},${servings},'${ingredients}','${steps}',` +
      ` ${is_public}, "${img_link}");`;

    const rows = await query(sql);
    // return true;
    return { success: true, insertId: rows.insertId };
  } catch (error) {
    console.table(error);
    return { success: false, insertId: null };
  }
};

exports.getAll = async (profileId) => {
  try {
    const sql =
      `SELECT *, IF ( profile_id = '${profileId}', 'true', 'false' ) AS owner` +
      ` FROM ${database_name}.recipe` +
      ` WHERE profile_id = '${profileId}' OR is_public = 1;`;

    const rows = await query(sql);

    return rows;
  } catch (error) {
    console.error(error); // TODO: this should be logged instead
    return false;
  }
};

exports.getById = async (profileId, recipeId) => {
  try {
    const sql =
      `SELECT *, IF ( profile_id = '${profileId}', 'true', 'false' ) AS owner` +
      ` FROM ${database_name}.recipe WHERE recipe_id = '${recipeId}'` +
      ` AND profile_id = ${profileId} OR recipe_id = '${recipeId}' ` +
      ` AND is_public = 1;`;

    console.log(`SQL REQUEST:\n`);
    console.log(sql);

    const rows = await query(sql);
    const row = rows[0];

    return row;
  } catch (error) {
    console.error(error); // TODO: this should be logged instead
    return false;
  }
};

exports.update = async (profileId, recipeId, values) => {
  try {
    const sql = recipeUpdateSqlQueryGenerator(profileId, recipeId, values);
    await query(sql);

    return true;
  } catch (error) {
    console.error(error); // TODO: this should be logged instead
    return false;
  }
};

exports.delete = async (profileId, recipeId) => {
  try {
    const sql =
      `DELETE FROM ${database_name}.recipe WHERE ` +
      `profile_id = '${profileId}' AND recipe_id = '${recipeId}';`;
    const response = await query(sql);

    return true;
  } catch (error) {
    console.error(error); // TODO: this should be logged instead
    return false;
  }
};

// Helper functions down here

const recipeUpdateSqlQueryGenerator = (profileId, recipeId, values) => {
  let sqlQuery = `UPDATE ${database_name}.recipe SET `;
  let keys = Object.keys(values);

  keys.forEach((key, index) => {
    if (typeof values[key] === "object") {
      values[key] = JSON.stringify(values[key]);
    }

    sqlQuery += `${key} = '${values[key]}'`;
    if (keys.length - 1 != index) sqlQuery += `, `;
  });

  sqlQuery += ` WHERE profile_id = ${profileId} AND recipe_id = ${recipeId};`;

  return sqlQuery;
};
