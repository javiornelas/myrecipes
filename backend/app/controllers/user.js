const query = require('../db/query');
const db = require('../db/connect');
const bcrypt = require('bcrypt');

const saltRounds = 10;

const database_name = 'my_simple_cookbook';

const register = async (username, password) => {
  try {
    const hashedPassword = await bcrypt.hash(password, saltRounds);
    const sql = `INSERT INTO ${database_name}.profile (username, password) values` +
              `('${username}', '${hashedPassword}');`;
  
    const result = await query(sql);
    return { success: true, result, message: 'User registered '};
    
  } catch (error) {
    if ( error.code === 'ER_DUP_ENTRY' ) {
      return { success: false, message: 'Username is taken' }
    }
    return { success: false, message: 'An error occurred' }

  }
};


const isAuthenticated = (req, res, next) => {
  if ( req.isAuthenticated() ) return next();

  return res.status(401).send('You are not authenticated');
};

module.exports = {register, isAuthenticated};