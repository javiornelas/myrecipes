const express = require('express');
const router = express.Router();

const passport = require('passport');
const LocalStrategy = require('passport-local');

const bcrypt = require('bcrypt');
const db = require('../db/connect');

const user = require('../controllers/user');

const database_name = 'my_simple_cookbook';

passport.use(new LocalStrategy( (username, password, done) => {

  const sql = `SELECT * FROM ${database_name}.profile WHERE username = ?`;

  db.query(sql, [ username ], async (error, row) => {
  
    if (error) return done(error);

    if (row.length < 1) return done(null, false, { message: 'Invalid login.' });

    try {
      const hashedPassword = row[0].password;
      const isAuthorized = await bcrypt.compare(password, hashedPassword);

      if (!isAuthorized) return done(null, false, { message: 'Invalid login.' });
      
      const user = {
         id: row[0].profile_id,
         username: row[0].username,
      }

      return done(null, user);

    } catch (error) {
      return done(error)
    }

  });

}));

passport.serializeUser( (user, done) => done(null, user) );

passport.deserializeUser( (user, done) => done(null, user) );

router.post('/login', passport.authenticate('local'), (req, res, next) => {
  res.send({"message": `You are at the login as ${req.user.username} with id ${req.user.id}`});
});

router.get('/logout', (req, res, next) => {
  req.logout( error => {
    if (error) return next(error);

    return res.send('You have successfully logged out');
  })
});

router.get('/authorize', user.isAuthenticated, (req, res, next) =>  {
  res.send({ "message": `You are authorized ${req.user.id}:${req.user.username}`});
});

module.exports = router;