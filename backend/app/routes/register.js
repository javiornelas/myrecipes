var express = require('express');
var router = express.Router();

const user = require('../controllers/user');

router.post('/register', async (req, res, next) => {
  const { username, password } = req.body;
  const response = await user.register(username, password);

  res.send( response );

  // res.send(`${username} has been registered`);
});

module.exports = router;