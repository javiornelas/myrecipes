var express = require('express');
var router = express.Router();

const recipe = require('../controllers/recipe');
const user = require('../controllers/user');


router.post('/create/', user.isAuthenticated, async (req, res, next) => {
  const createdRecipe = await recipe.create(req.user.id, req.body.recipe);

  if ( !createdRecipe.success ) {
    return res.status(500).json({
      message: 'Failed to create recipe.',
      id: createdRecipe.insertId,
    });
  };
  
  return res.status(201).json({
    status: 201,
    message: 'New Recipe Created',
    id: createdRecipe.insertId,
  });
});


router.get('/', user.isAuthenticated, async (req, res, next) => {
  const retrievedRecipes = await recipe.getAll(req.user.id);

  if ( !retrievedRecipes ) {
    return res.status(500).send('Failed to retrieve recipe');
  };

  return res.status(200).send(retrievedRecipes);
});


router.get('/:recipeId', user.isAuthenticated, async (req, res, next) => {
  const { recipeId } = req.params;
  const profileId = req.user.id;

  const retrievedRecipe = await recipe.getById(profileId, recipeId);

  if ( !retrievedRecipe ) {
    return res.status(500).send('Failed to retrieve recipe');
  };

  return res.status(200).send(retrievedRecipe);
});


router.post('/update/:recipeId', user.isAuthenticated, async (req, res, next) => {
  const profileId = req.user.id;
  const { recipeId } = req.params;
  const values = req.body;

  const recipeUpdated = await recipe.update(profileId, recipeId, values);

  if ( !recipeUpdated ) {
    return res.status(500).json({
      status: 500,
      message: 'Failed to update recipe',
      success: false,
    });
  };

  return res.status(200).json({
    status: 200,
    message: 'Recipe Saved',
    success: true,
  });
});


router.delete('/delete/:recipeId', user.isAuthenticated, async (req, res, next) => {
  const profileId = req.user.id;
  const { recipeId } = req.params;

  const recipeDeleted = await recipe.delete(profileId, recipeId);

  if ( !recipeDeleted ) {
    return res.status(500).json({
      status: 500,
      message: 'Failed to Delete Recipe',
      success: false,
    });
  };

  return res.status(200).json({
    status: 200,
    message: 'Recipe Delete',
    success: true,
  });
});


module.exports = router;