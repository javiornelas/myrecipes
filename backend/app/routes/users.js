var express = require('express');
var router = express.Router();

const user = require('../controllers/user');

/* GET users listing. */
router.get('/', user.isAuthenticated, function(req, res, next) {
  console.table(req);
  const {username, id} = req.user;

  res.send({id, username});
});

module.exports = router;
