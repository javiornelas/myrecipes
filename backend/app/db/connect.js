const mysql = require("mysql2");
//const settings = require('./settings.json');

const settings = {
  connectionLimit: process.env.DB_SETTING_CONNECTION_LIMIT,
  host: process.env.DB_SETTING_MYSQL_HOST,
  user: process.env.DB_SETTING_MYSQL_USER,
  password: process.env.DB_SETTING_MYSQL_USER_PASSWORD,
  database: process.env.DB_SETTING_MYSQL_DB,
};

let database;

const connect = () => {
  if (database === undefined) {
    database = mysql.createPool(settings);
  }
  return database;
};

module.exports = connect();
