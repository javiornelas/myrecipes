const db = require('./connect');

const databaseQuery = async (query, values = []) => {
  return new Promise( (resolve, reject) => {
    db.query(query, values,(error, results, fields) => {
      if (error) reject(error);

      resolve(results);
    });
  });
};

module.exports = databaseQuery;