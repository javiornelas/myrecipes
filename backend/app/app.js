var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const cors = require('cors');

const passport = require('passport');
const session = require('express-session');

var indexRouter = require('./routes/index');
const authRouter = require('./routes/auth');
const registerRouter = require('./routes/register');
const recipeRouter = require('./routes/recipe');
var usersRouter = require('./routes/users');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: 'fkIRV3bZRv0Zro3N6ISXOSecLBvN1D',
    resave: false,
    saveUninitialized: false,
}));

app.use(passport.authenticate('session'));

let FRONTEND_URL = null;
let HTTP_PROTOCOL = null;

const isIPAddress = (ip) => {
    const ipAddressRegex = /^(\d{1,3}\.){3}\d{1,3}$/;
    return ipAddressRegex.test(ip);
}

if (process.env.HTTPS === 'true') HTTP_PROTOCOL = 'https://';
else HTTP_PROTOCOL = 'http://';

if (isIPAddress(process.env.FRONTEND_URL)) {
    FRONTEND_URL = 
        HTTP_PROTOCOL +
        process.env.FRONTEND_URL + ':' +
        process.env.FRONTEND_PORT
}
else if (process.env.CORS_USE_PORT === 'true') {
    FRONTEND_URL = 
        HTTP_PROTOCOL +
        process.env.FRONTEND_URL + ':' +
        process.env.FRONTEND_PORT
} else {
    FRONTEND_URL = 
        HTTP_PROTOCOL +
        process.env.FRONTEND_URL
};

console.log(`CORS URL: ${FRONTEND_URL}`);

const corsOptions = {
    origin: `${FRONTEND_URL}`,
    credentials: true,
}

app.use(cors(corsOptions));

app.use('/', indexRouter);
app.use('/', authRouter);
app.use('/', registerRouter);
app.use('/recipe', recipeRouter);
app.use('/user', usersRouter);

module.exports = app;
