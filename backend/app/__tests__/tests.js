const server = require("../app.js");
const supertest = require("supertest");
const requestWithSupertest = supertest(server);

//  KEY : TYPE
const recipe_format_json = {
  name: "string",
  cook_time: "number",
  prep_time: "number",
  servings: "number",
  calories: "number",
  ingredients: "object",
  steps: "object",
  isPublic: "boolean",
};

const first_recipe_to_check = {
  name: "Beans",
  cook_time: 10,
  prep_time: 10,
  servings: 2,
  calories: 300,
  ingredients: {},
  steps: {},
  isPublic: true,
};

describe("Recipe Functions", () => {
  const test_check_recipe_types = (
    first_recipe_to_check,
    recipe_format_json
  ) => {
    return true;
  };

  test("Check Recipe Types", () => {
    expect(test_check_recipe_types()).toBe(true);
  });
});
