# [My Simple Cookbook](https://mysimplecookbook.javiornelas.com)

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/javiornelas/my-simple-cookbook)

## _A simple cookbook project_

My Simple Cookbook is a web-based cookbook that allows users to create an account and store their personal recipes. Users can easily create, edit, and delete recipes, and choose to make them public for others to view. This application is built using React for the front-end, Express for the back-end, and MySQL for the database.

The project was developed as a demonstration and may not include all necessary features, such as password reset functionality.

[My Simple Cookbook Demo](https://mysimplecookbook.javiornelas.com)

## Technologies
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![Bootstrap](https://img.shields.io/badge/bootstrap-%23563D7C.svg?style=for-the-badge&logo=bootstrap&logoColor=white)
![Express.js](https://img.shields.io/badge/express.js-%23404d59.svg?style=for-the-badge&logo=express&logoColor=%2361DAFB)
![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)


## Setup with Docker Compose

Local installation is made to work with docker compose. The docker-compose.yaml file is present in the base directory and must be edited to before use.

Clone the directory:

`git clone https://gitlab.com/javiornelas/my-simple-cookbook.git`

Navigate into base directory:

`cd my-simple-cookbook`

Modify 'docker-compose.yaml' file to fit local environment using favorite editor

`vim docker-compose.yaml`

Start the web application with docker compose.

`docker compose up`
