import React, { useState } from 'react';
import { LoginForm } from '../components/Forms/LoginForm';
import { RegisterForm } from '../components/Forms/RegisterForm';
import useAuth from '../custom_hooks/useAuth';
import Button from 'react-bootstrap/Button';

export default function LoginPage() {
  const [ togglePage, setTogglePage ] = useState(true);

  const { login } = useAuth();

  const handleDemoUserLogin = async () => {
    return await login('demo_user', 'demo');
  };

  const onTogglePage = (e) => {
    if ( !e.target.className.includes('active') ) {
      setTogglePage( toggle => !toggle );
    };
    return;
  };

  return (
    <React.Fragment>
    <div className="position-absolute bottom-50 start-50 translate-middle-x">

      <div className="" style={{ "backgroundColor": "", "width": "300px" }}>

        <ul className="nav nav-pills my-2">

          <li className="nav-item">
            <button className={ togglePage ? 'nav-link active' : 'nav-link' } onClick={ e => onTogglePage(e) }> LOGIN </button>
          </li>

          <li className="nav-item">
            <button className={ togglePage ? 'nav-link' : 'nav-link active'} onClick={ e => onTogglePage(e) }>REGISTER</button>
          </li>

        </ul>

        <div className="col-12 mx-auto">
          <h2>My Simple Cookbook</h2>
        </div>

        { togglePage ? <LoginForm /> : <RegisterForm /> }
        
        <hr />
        
        <Button 
          variant="primary"
          className="my-2"
          onClick={handleDemoUserLogin}
        >
          Guest Login
        </Button>

      </div>

    </div>

    </React.Fragment>
  );
};
