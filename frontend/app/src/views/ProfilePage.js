import './MySimpleCookbook.css';

import Header from '../components/Header';

export default function ProfilePage() {
  return (
    <div className="MySimpleCookbook">
      <Header/> <br /> <br />
      <div className="container-fluid">
        <h1>Profile Pages</h1>
      </div>
    </div>
  );
};
