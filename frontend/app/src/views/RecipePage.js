import React, { useEffect, useState, useRef } from 'react';
import { useParams, useNavigate } from "react-router-dom";
import { getRecipe } from '../api/recipe';
import PageLayout from "../components/pageLayout";

import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';

import { ChevronLeft, PencilSquare } from 'react-bootstrap-icons';

import { Ingredients } from "../components/ingredients";
import { RecipeSteps } from "../components/recipeSteps";

// import pizzaImage from '../data/pizza_background.jpg';

import "../css/main.css";
import { LoadingScreen } from '../components/loadingComponent';

// const RecipeImage = (props) => {

//   const backgroundStyle = {
//     backgroundImage: `url(${props.image})`,
//     backgroundRepeat: "no-repeat",
//   }

//   return (
//     <div className="container-fluid">
//       <img src={props.image} />
//       {/* <RecipeDefaultImage width="1rem" /> */}
//     </div>
//   );
// };

const RecipeTitle = (props) => {
  const { name, cook_time, prep_time, calories, servings } = props.recipe;

  let text = `${servings} servings | ${prep_time} mins | `;
  text += `${cook_time} mins | ${calories} calories`;

  return (
    <div className="container-fluid">
      <h4 className="text-center">{ name }</h4>
      <p className="text-center text-muted"> { text } </p>
    </div>
  );
};


const BackButton = () => {
  let navigate = useNavigate();
  return (
    <Button variant="secondary"
      onClick={ () => navigate("/recipes") }
    >
      <ChevronLeft />
    </Button>
  )
}

const RecipeEditButton = (props) => {
  let navigate = useNavigate();
  const updateRecipeUrl = `/recipe/${props.id}/edit`
  const isDisabled = ( props.isOwner === 'true' ) ? false : true ;

  return (
    <Button onClick={ () => navigate(updateRecipeUrl) } disabled={ isDisabled } >
      <PencilSquare />
    </Button>
  );
};

export default function RecipePage() {
  
  let params = useParams();
  const hasLoaded = useRef(false);
  const [ recipe, setRecipe ] = useState('');

  const getRecipeById = async (id) => {
    const recipe = await getRecipe(id);
    setRecipe( () => recipe );
  };

  useEffect( () => {
    if ( !hasLoaded.current ) {
      getRecipeById(params.recipeId);
      hasLoaded.current = true;
    }
  }, [recipe, params.recipeId]);


  const content = () => {
    return (
      <div className="container-fluid">

        <ButtonGroup size="sm" className="mb-2 mt-2" >
          <BackButton />
          <RecipeEditButton 
            id={ params.recipeId }
            isOwner={ recipe.owner }
          />
        </ButtonGroup>

        {/* <RecipeImage image={ pizzaImage } /> */}

        <RecipeTitle recipe={ recipe } />

        <Ingredients ingredients={ recipe.ingredients }/>

        <RecipeSteps steps={ recipe.steps } />

      </div>
    );    
  }

  return (
    <PageLayout>
      { hasLoaded.current ? content() : <LoadingScreen />}
      < br />
    </PageLayout>
  );
};