import React from 'react';
import PageLayout from '../components/pageLayout';

export default function SettingsPage() {
  return (
    <PageLayout>
      <div className="container">
        <h1>Settings Page</h1>
      </div>
    </PageLayout>
  );
}