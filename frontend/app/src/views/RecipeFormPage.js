import React, { useEffect, useRef, useState } from "react";
import { useParams } from 'react-router-dom';
import PageLayout from "../components/pageLayout";
import RecipeForm from "../components/recipeForm";

import { getRecipe } from '../api/recipe';

const AddRecipeForm = (props) => {
  return (
    <>
      <h2>Create New Recipe</h2>
      <RecipeForm />
    </>
  );
};

const EditRecipeForm = ({ recipe }) => {
  return (
    <>
      <h2 className="header">Editing Recipe</h2>
      <RecipeForm recipe={ recipe } />
    </>
  );
};

export default function RecipeFormPage(props) {
  let params = useParams();

  const hasLoaded = useRef(false);
  const [ recipe, setRecipe ] = useState('');

  const getRecipeById = async (id) => {
    try {
      const recipe = await getRecipe(id);
      setRecipe( () => recipe );
    } catch (error) {
      console.error(`Hiding the error:\n${error}`);
    }
  };

  useEffect( () => {
    if ( !hasLoaded.current ) {
      getRecipeById(params.recipeId);
      hasLoaded.current = true;
    }
  }, [recipe, params.recipeId]);  

  return (
    <PageLayout>
      { recipe !== '' 
        ? <EditRecipeForm recipe={ recipe } />
        : <AddRecipeForm />
      }
    </PageLayout>
  );
};