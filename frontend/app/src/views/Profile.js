
import React, {useState, useRef, useEffect} from 'react';
import Container from 'react-bootstrap/Container';

import PageLayout from '../components/pageLayout';

import {getProfile} from '../api/user';

export default function ProfilePage() {
  const [profile, setProfile] = useState({});
  const hasLoaded = useRef(false);

  useEffect(() => {
    getUserProfile();
  }, [])

  const getUserProfile = async () => {
    const profile = await getProfile();
    setProfile( () => profile);
  }

  return (
    <PageLayout>
      <Container>
        <h1 className="header">Profile Page</h1>
        <h5>User: {profile.username}</h5>
      </Container>
    </PageLayout>
  );
};