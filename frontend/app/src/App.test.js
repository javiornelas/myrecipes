import { render, screen } from '@testing-library/react';
import App from './App';

test('renders Test div', () => {
  render(<div>Test</div>);
  const linkElement = screen.getByText(/Test/i);
  expect(linkElement).toBeInTheDocument();
});
