import React, { useState, useRef, useEffect } from 'react';
import './css/main.css';

import PageLayout from './components/pageLayout';
import ButtonToolbar from './components/buttonsRow';
import RecipeList from './components/recipeList';

import { getAll } from './api/recipe';


function App() {
  
  const FILTER = {
    ALL: 'ALL',
    PUBLIC: 'PUBLIC',
    OWN: 'OWN',
    DEFAULT: 'ALL',
  };

  const [ filter, setFilter ] = useState(FILTER.DEFAULT);
  const changeFilter = (filterState) => {
    setFilter( () => filterState );
  };

  const hasLoaded = useRef(false);
  const [ recipeList, setRecipeList ] = useState('');

  useEffect( () => {
    if ( !hasLoaded.current ) {
      getAllRecipes();
      hasLoaded.current = true;
    }
  }, [recipeList]);

  const getAllRecipes = async () => {
    const recipes = await getAll();
    setRecipeList( () => recipes  );
  };

  return (
    <PageLayout>
      <ButtonToolbar onChangeFilter={ changeFilter } />
      <RecipeList list={recipeList} filter={ filter }/>
    </PageLayout>
  );
};

export default App;