import { useNavigate } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton  from 'react-bootstrap/DropdownButton';

const BUTTON = {
  ADD_RECIPE: { text: "Add Recipe", path: "/recipe/add" },
  FILTER: { text: "List", },
};

const ButtonToolbar = (props) => {
  const navigate = useNavigate();

  const navigateTo = (location) => {
    navigate(location);
  }

  return (
    <ButtonGroup size="sm" className="mt-2 mb-2" >

      <Button onClick={() => navigateTo(BUTTON.ADD_RECIPE.path) } > 
          {BUTTON.ADD_RECIPE.text}
      </Button>

      <DropdownButton as={ ButtonGroup }
        title={BUTTON.FILTER.text}
        variant="secondary"
      >
        <Dropdown.Item onClick={ () => props.onChangeFilter('ALL') } >
          All
        </Dropdown.Item>

        <Dropdown.Item onClick={ () => props.onChangeFilter('PUBLIC') } >
          Public
        </Dropdown.Item>

        <Dropdown.Item onClick={ () => props.onChangeFilter('OWN') } >
          My Recipes
        </Dropdown.Item>

      </DropdownButton>

    </ButtonGroup>
  );
};

export default ButtonToolbar;