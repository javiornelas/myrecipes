import React from "react";
import Header from "./header";

const PageLayout = (props) => {
  return (
    <React.Fragment>
      <Header/>
      <div className="container">
          {props.children}
      </div>
    </React.Fragment>
  );
}

export default PageLayout;