import { useState, useEffect } from "react";

import { register, getStatusMessage } from "../../api/register";

export const RegisterForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [status, setStatus] = useState("");
  useEffect(() => {
    if (status === "") return;

    const statusMessage = document.getElementById("status-message");

    statusMessage.classList.remove(...statusMessage.classList);
    void statusMessage.offsetWidth;
    statusMessage.classList.add("show-status-message");
  }, [status]);

  const inputBoxClass = "form-control";

  const passwordsMatch = (e) => {
    if (password !== confirmPassword) return false;
    return true;
  };

  const handleRegistration = async (e) => {
    e.preventDefault();
    if (!passwordsMatch(e)) {
      const statusMessage = getStatusMessage("passwordMismatch");
      setStatus(() => statusMessage);
      return;
    }

    const registrationResponse = await register(username, password);
    setStatus(() => registrationResponse.message);
  };

  return (
    <form className="row g-2">
      <div className="col-12 form-floating">
        <input
          type="text"
          className={inputBoxClass}
          id="username"
          placeholder="&nbsp; username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <label htmlFor="username"> &nbsp; Username </label>
      </div>

      <div className="col-12 form-floating">
        <input
          htmlFor="password"
          type="password"
          id="password"
          placeholder="password"
          autoComplete="on"
          className={inputBoxClass}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <label htmlFor="password"> &nbsp; Password</label>
      </div>

      <div className="col-12 form-floating">
        <input
          htmlFor="password"
          type="password"
          id="confirm-password"
          placeholder="Confirm Password"
          autoComplete="on"
          className={inputBoxClass}
          value={confirmPassword}
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
        <label htmlFor="password"> &nbsp; Confirm Password</label>
      </div>

      <div className="hidden-status-message" id="status-message">
        <span className="d-block fw-bolder text-danger">{status}</span>
      </div>

      <div className="col-12">
        <button
          type="submit"
          className="btn btn-primary"
          onClick={(e) => handleRegistration(e)} // this should actually handle register submission
        >
          Register
        </button>
      </div>
    </form>
  );
};
