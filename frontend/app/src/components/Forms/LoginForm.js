import { useState, useEffect } from 'react';

import useAuth from '../../custom_hooks/useAuth';
import { getStatusMessage } from '../../api/login';

import '../../App.css';

export const LoginForm = () => {

  const [ username, setUsername ] = useState('');
  const [ password, setPassword ] = useState('');
  const [ status, setStatus ] = useState('');
  
  const { login } = useAuth();

  const inputBoxClass = "form-control";

  useEffect( () => {
    if ( status === '' ) return;

    const statusMessage = document.getElementById("status-message");
    
    statusMessage.classList.remove(...statusMessage.classList);
    void statusMessage.offsetWidth;
    statusMessage.classList.add("show-status-message");
    
  }, [status]);

  const handleLogin = async (e) => {
    e.preventDefault();
    
    const responseStatus = await login(username, password);
    const statusMessage = getStatusMessage(responseStatus);
    setStatus( () => statusMessage );

  };

  return (
    <form className="row g-2" id="login-form">
    
    <div className="col-12 form-floating">
      <input required type="text" className={ inputBoxClass }
        id="username" placeholder="&nbsp; username" 
        onChange={ (e) => setUsername(e.target.value) }
      />
      <label htmlFor="username"> &nbsp; Username </label>
    </div>

    <div className="col-12 form-floating">
      <input required type="password" id="password"
      placeholder="password" autoComplete="on"
      className={ inputBoxClass }
      onChange={ (e) => setPassword(e.target.value) }
      />
      <label htmlFor="password"> &nbsp; Password</label>
    </div>

    <div className="hidden-status-message" id="status-message">
      <span className="d-block fw-bolder text-danger">{ status }</span>
    </div>

    <div className="col-12">
      <button type="submit" className="btn btn-primary"
        onClick={ handleLogin }
        value="Login">Login</button>
      {/* <button/> */}

    </div>

    </form>
  );
};
