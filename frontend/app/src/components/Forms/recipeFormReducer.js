export const initialState = {
  recipe: {
    id: "",
    name: "",
    prep_time: 0,
    cook_time: 0,
    servings: 1,
    calories: 0,
    isPublic: 0,
    img_link: "",
  },

  steps: {
    list: [],
    currentStepIndex: undefined,
    title: "",
    substeps: [],
    substepText: "",
    editing: { // Adding a place for edited step to be saved
      currentStepIndex: undefined,
      title: "",
      substeps: [],
      substepText: "",
    },
    unsaved: { // Adding a place for unsaved (not yet added) step to be saved
      currentStepIndex: undefined,
      title: "",
      substeps: [],
      substepText: "",
    },
  },

  ingredients: {
    list: [],
    name: "",
    quantity: 1,
    quantityType: "",
  },
};

export const ACTIONS = {
  LOAD: "load",

  ON_PUBLIC_CHANGE: "on-public-change",

  ON_CHANGE: "on-change",
  ADD_INGREDIENT: "add-ingredient",
  REMOVE_INGREDIENT: "remove-ingredient",

  MOVE_UP_INGREDIENT: "move-up-ingredient",
  MOVE_DOWN_INGREDIENT: "move-down-ingredient",

  MOVE_UP_STEP: "move-up-step",
  MOVE_DOWN_STEP: "move-down-step",
  SAVE_STEP: "save-step",
  EDIT_STEP: "edit-step",
  UNDO_STEP_CHANGES: "undo-step-changes",
  RESET_STEPS_TMP_INDEX: "reset-steps-tmp-index",
  CLOSE_STEP: "close-step",

  ADD_SUBSTEP: "add-substep",
  REMOVE_SUBSTEP: "remove-substep",

  MOVE_UP_SUBSTEP: "move-up-substep",
  MOVE_DOWN_SUBSTEP: "move-down-substep",
  SAVE_SUBSTEP: "save-substep",
  // EDIT_SUBSTEP: 'edit-substep',

  SAVE_RECIPE: "save-recipe",
};

export const reducer = (state, action) => {
  switch (action.type) {
    case ACTIONS.ON_PUBLIC_CHANGE: {
      const { checked } = action.payload;
      const checkedValue = checked ? 1 : 0;
      return {
        ...state,
        recipe: {
          ...state.recipe,
          isPublic: checkedValue,
        },
      };
    }

    case ACTIONS.ON_CHANGE: {
      const { name, value } = action.payload.event.target;
      const { key } = action.payload;
      return { ...state, [key]: { ...state[key], [name]: value } };
    }

    case ACTIONS.CLOSE_STEP: {
      return {
        ...state,
        steps: {
          ...state.steps,
          substeps: [],
          title: "",
          substepText: "",
          currentStepIndex: undefined,
        },
      };
    }

    case ACTIONS.ADD_INGREDIENT: {
      const { name, quantity, quantityType } = action.payload;
      const newIngredient = { name, quantity, quantityType };

      if (!name || !quantity) {
        return state;
      }

      return {
        ...state,
        ingredients: {
          name: "",
          quantity: 1,
          quantityType: "",
          list: [...state.ingredients.list, newIngredient],
        },
      };
    }

    case ACTIONS.EDIT_INGREDIENT: {
      // NOT DOING THIS YET.
      // const { index } = action.payload;
      return { ...state };
      // const { title, substeps } = action.payload.step;

      // return {
      //   ...state,
      //   steps: {
      //     ...state.steps,
      //     currentStepIndex: index,
      //     title,
      //     substeps
      //   }
      // };
    }

    case ACTIONS.REMOVE_INGREDIENT: {
      const { list: oldList } = state.ingredients;
      const { index } = action.payload;
      const list = oldList.filter((_i, j) => index !== j);

      return {
        ...state,
        ingredients: { ...state.ingredients, list },
      };
    }

    case ACTIONS.MOVE_UP_INGREDIENT: {
      const { index } = action.payload;
      if (index === 0) return { ...state };

      const { list: oldList } = state.ingredients;
      const list = arraySwap([...oldList], index, index - 1);

      return {
        ...state,
        ingredients: { ...state.ingredients, list },
      };
    }

    case ACTIONS.MOVE_DOWN_INGREDIENT: {
      const { index } = action.payload;
      const { list: oldList } = state.ingredients;
      if (index === oldList.length - 1) return { ...state };

      const list = arraySwap([...oldList], index, index + 1);

      return {
        ...state,
        ingredients: { ...state.ingredients, list },
      };
    }

    case ACTIONS.MOVE_UP_STEP: {
      const { index } = action.payload;
      if (index === 0) return { ...state };

      const { list: oldList } = state.steps;
      const list = arraySwap([...oldList], index, index - 1);

      return {
        ...state,
        steps: { ...state.steps, list },
      };
    }

    case ACTIONS.MOVE_DOWN_STEP: {
      const { index } = action.payload;
      const { list: oldList } = state.steps;
      if (index === oldList.length - 1) return { ...state };

      const list = arraySwap([...oldList], index, index + 1);

      return {
        ...state,
        steps: { ...state.steps, list },
      };
    }

    case ACTIONS.SAVE_STEP: {
      const { currentStepIndex: index } = state.steps;

      const { title, substeps, list: oldList } = action.payload.steps;
      const step = { title, substeps };
      let list = [...oldList];

      if (index !== undefined) {
        list[index] = step;
      } else {
        list.push(step);
      }

      return {
        ...state,
        steps: { title: "", substeps: [], substepText: "", list },
      };
    }

    case ACTIONS.EDIT_STEP: {
      const { index } = action.payload;
      const { title, substeps } = action.payload.step;

      return {
        ...state,
        steps: {
          ...state.steps,
          currentStepIndex: index,
          title,
          substeps,
        },
      };
    }

    case ACTIONS.UNDO_STEP_CHANGES: {
      const { title, substeps } = action.payload.initialStepValues;

      return {
        ...state,
        steps: {
          ...state.steps,
          title,
          substeps,
        },
      };
    }

    case ACTIONS.MOVE_UP_SUBSTEP: {
      const { index } = action.payload;
      if (index === 0) return { ...state };

      const { substeps: oldSubsteps } = state.steps;
      
      const substeps = arraySwap([...oldSubsteps], index, index - 1);

      const newState = {
        ...state,
        steps: { ...state.steps, substeps },
      };


      return newState;
    }

    case ACTIONS.MOVE_DOWN_SUBSTEP: {
      const { index } = action.payload;
      const { substeps: oldSubsteps } = state.steps;
      if (index === oldSubsteps.length - 1) return { ...state };

      const substeps = arraySwap([...oldSubsteps], index, index + 1);


      const newState = {
        ...state,
        steps: { ...state.steps, substeps },
      };
      return newState;
    }

    case ACTIONS.ADD_SUBSTEP: {
      return {
        ...state,
        steps: {
          ...state.steps,
          substepText: "",
          substeps: [...state.steps.substeps, action.payload.substepText],
        },
      };
    }

    case ACTIONS.REMOVE_SUBSTEP: {
      const { substeps: oldSubsteps } = state.steps;
      const { index } = action.payload;

      const substeps = oldSubsteps.filter((_i, j) => index !== j);

      return {
        ...state,
        steps: { ...state.steps, substeps },
      };
    }

    case ACTIONS.SAVE_SUBSTEP: {
      const { index, substep } = action.payload;
      const { substeps } = state.steps;
      let newSubsteps = [...substeps];
      newSubsteps[index] = substep;

      return {
        ...state,
        steps: {
          ...state.steps,
          substeps: newSubsteps,
        },
      };
    }

    case ACTIONS.SAVE_RECIPE: {
      return {
        ...state,
      };
    }

    case ACTIONS.LOAD: {
      return action.payload;
    }

    default: {
      throw new Error();
    }
  }
};

const arraySwap = (arr, first, second) => {
  const temp = arr[first];
  arr[first] = arr[second];
  arr[second] = temp;
  return arr;
};
