import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export const DeleteRecipeModal = (props) => {
  const {
    showDeleteRecipeModal,
    setShowDeleteRecipeModal,
    handleDeleteRecipe,
    isDeleting,
  } = props;

  const handleCloseModal = () => {
    setShowDeleteRecipeModal(false);
  };

  return (
    <Modal show={showDeleteRecipeModal} onHide={handleCloseModal}>
      <Modal.Header closeButton>
        <Modal.Title>Delete Recipe</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Are you sure you want to delete recipe?
      </Modal.Body>
      <Modal.Footer>
        {isDeleting ? <></>
        :  
        <Button variant="secondary" onClick={handleCloseModal}>
            Cancel
        </Button>
        }
        <Button variant="danger" onClick={handleDeleteRecipe}>
            {isDeleting ? 'Deleting...' : 'Delete Recipe'}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};