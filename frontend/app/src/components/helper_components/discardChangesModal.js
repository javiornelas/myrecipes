import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export const DiscardChangesModal = (props) => {
  const {
    showDiscardChangesModal,
    setShowDiscardChanges,
    handleDiscardChangesDiscard,
    handleDiscardChangesSave,
  } = props;

  const handleCloseModal = () => {
    setShowDiscardChanges(false);
  };

  return (
    <Modal show={showDiscardChangesModal} onHide={handleCloseModal}>
      <Modal.Header closeButton>
        <Modal.Title>Discard Changes</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Are you sure you want to discard changes?
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleDiscardChangesDiscard}>
          Discard Changes
        </Button>
        <Button variant="primary" onClick={handleDiscardChangesSave}>
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  );
};