import React from "react";
import { useState, useEffect, useRef, useReducer } from "react";
import { useNavigate } from "react-router-dom";
import { RecipeSection } from "./recipeSection";
import { StepSection } from "./stepSection";
import { IngredientSection } from "./ingredientSection";

import { DiscardChangesModal } from "./helper_components/discardChangesModal";
import { DeleteRecipeModal } from "./helper_components/deleteRecipeModal";

import { initialState, reducer } from "./Forms/recipeFormReducer";
import { create, edit, deleteRecipe } from "../api/recipe";

import Button from "react-bootstrap/Button";

import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { NavItem } from "react-bootstrap";

const changeRecipeFormat = (state, formatType) => {
  if (formatType === "backend") {
    let recipe = {
      recipe: {
        ...state.recipe,
        is_public: state.recipe.isPublic,
        ingredients: state.ingredients.list,
        steps: state.steps.list,
      },
    };

    delete recipe.recipe["isPublic"];
    return recipe;
  }

  if (formatType === "frontend") {
    return {
      recipe: {
        id: state.recipe_id,
        name: state.name,
        prep_time: state.prep_time,
        cook_time: state.cook_time,
        servings: state.servings,
        calories: state.calories,
        isPublic: state.is_public,
      },
      steps: {
        list: state.steps,
        substeps: [],
        title: "",
        substepText: "",
        currentStepIndex: undefined,
      },
      ingredients: {
        list: state.ingredients,
        name: "",
        quantity: 1,
        quantityType: "",
      },
    };
  }

  return state;
};

const getInitialState = (recipe) => {
  if (recipe === undefined) return initialState;

  return changeRecipeFormat(recipe, "frontend");
};

export default function RecipeForm(props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const isStateUpdated = useRef(false);
  const readyToDetectUpdates = useRef(false);

  useEffect(() => {
    if (readyToDetectUpdates.current) {
      isStateUpdated.current = true;
    }
    
    if (hasLoaded.current && !readyToDetectUpdates.current) {
      readyToDetectUpdates.current = true;
    }
  }, [state])

  const hasLoaded = useRef(false);
  let navigate = useNavigate();

  const [message, setMessage] = useState("");

  const createRecipe = async () => {
    const recipe = changeRecipeFormat(state, "backend");
    const response = await create(recipe);

    const { status, id } = response;
    if (status === 201) {
      const recipeUrl = `/recipe/${id}`;
      navigate(recipeUrl);
    } // else, should show error
  };

  const saveRecipe = async () => {
    const { recipe } = changeRecipeFormat(state, "backend");

    const response = await edit(recipe);
    const { status, message: responseMessage } = response;

    setMessage(() => responseMessage);

    // const saveMessageModal = document.getElementById("messageModal");
    // const modalObject = new Modal(saveMessageModal);
    // modalObject.show();
    navigate(`/recipe/${state.recipe.id}`);
  };

  useEffect(() => {
    if (!hasLoaded.current) {
      dispatch({ type: "load", payload: getInitialState(props.recipe) });
      hasLoaded.current = true;
    }
  }, [props.recipe]);

  const CreateButton = () => {
    return (
      <Button
        variant="primary"
        onClick={createRecipe}
      >
        Create Recipe
      </Button>
    );
  };

  const SaveButton = () => {
    return (
      <Button
        variant="primary"
        onClick={saveRecipe}
      >
        Save Recipe
      </Button>
    );
  };


  const handleDiscardButton = () => {
    if (!isStateUpdated.current) navigate('/recipes');

    setShowDiscardChanges(true);
  }
  
  const [showDiscardChangesModal, setShowDiscardChanges] = useState(false);
  const handleDiscardChangesDiscard = () => {
    navigate('/recipes');
  };

  const handleDiscardChangesSave = () => {
    setShowDiscardChanges(false);
    saveRecipe();
  };

  const DiscardButton = () => {
    return (
      <Button variant="outline-danger" onClick={handleDiscardButton}>
        Discard Changes
      </Button>
    );
  }


  const [isDeleting, setIsDeleting] = useState(false);
  
  const [showDeleteRecipeModal, setShowDeleteRecipeModal] = useState(false);
  const handleDeleteRecipe = () =>  {
    setIsDeleting(true);
    if (state.recipe.id === "") return navigate('/recipes');

    deleteRecipe(state.recipe.id);

    setTimeout(() => {
      navigate('/recipes');
    }, 500);

  };

  const handleDeleteButton = () => {
    setShowDeleteRecipeModal(true);
  }

  const DeleteButton = () => {
    return (
      <Button variant="danger" onClick={handleDeleteButton}>
        Delete Recipe
      </Button>
    )
  }

  return (
    <form className="row g-3">
      <RecipeSection recipe={state.recipe} dispatch={dispatch} />
      <hr />

      <IngredientSection ingredients={state.ingredients} dispatch={dispatch} />
      <hr />

      <StepSection steps={state.steps} dispatch={dispatch} />
      <hr />

      <Container fluid className="mb-3">
        <Row className="">
          <Col lg={{ span: 2 }} className="d-grid mb-2">
            {state.recipe.id === "" ? <CreateButton /> : <SaveButton />}
          </Col>

          <Col lg={{ span: 3, offset: 4 }} className="d-grid mb-2">
            { state.recipe.id === "" ? <></> : <DiscardButton /> }
          </Col>

          <Col lg={{ span: 3 }} className="d-grid gap-2 mb-2">
            <DeleteButton />
          </Col>
        </Row>
      </Container>

      <MessageModal message={message} />
      
      <DiscardChangesModal 
        showDiscardChangesModal={ showDiscardChangesModal }
        setShowDiscardChanges={ setShowDiscardChanges }
        handleDiscardChangesDiscard={ handleDiscardChangesDiscard }
        handleDiscardChangesSave={ handleDiscardChangesSave }
      />

      <DeleteRecipeModal 
        showDeleteRecipeModal={ showDeleteRecipeModal }
        setShowDeleteRecipeModal={ setShowDeleteRecipeModal }
        handleDeleteRecipe={ handleDeleteRecipe }
        isDeleting={ isDeleting }
        setIsDeleting={ setIsDeleting }
      />
    </form>
  );
}

const MessageModal = ({ message }) => {
  return (
    <div className="modal" tabIndex="-1" id="messageModal">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{message}</h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          {/* <div className="modal-body">
            <p>Recipe Saved</p>
          </div> */}
          {/* <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" className="btn btn-primary">Save changes</button>
          </div> */}
        </div>
      </div>
    </div>
  );
};
