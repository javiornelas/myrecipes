import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import { Link, useNavigate, useLocation } from "react-router-dom";
import useAuth from '../custom_hooks/useAuth';

const HomeNav = (props) => {
  const navigate = useNavigate();
  
  const location = useLocation();
  const appLink = () => {
    if ( location.pathname === "/recipes" ) return "#";
    return "/recipes";
  }

  const { logout } = useAuth();
  const handleLogout = () => {
    logout().then(() => {
      navigate("/");
    });
  };

  return (
    <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
      <Container fluid>
        <Navbar.Brand href={ appLink() } >My Simple Cookbook</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <NavbarLink href="/recipes" text="Recipes" />
            <NavbarLink href="/profile" text="Profile" />
            <NavbarLink href="/settings" text="Settings" />
          </Nav>
          <Nav>
            <Button variant="outline-danger" onClick={handleLogout}>
              Logout 
            </Button>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

const NavbarLink = ({ href, text }) => {
  return (
    <Link className="nav-link pt-2 pb-2" to={href}> {text} </Link>
  );
}

export default HomeNav;
