import { useState, useRef, useEffect } from 'react';

import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import { Modal } from 'react-bootstrap';

import { ArrowUp, ArrowDown } from 'react-bootstrap-icons';

import { ACTIONS } from './Forms/recipeFormReducer';
import { DiscardChangesModal } from './helper_components/discardChangesModal';


const StepList = ({ handleStepModalOpen, steps, dispatch }) => {
  if ( steps.list.length === 0 ) return (<span>Add Steps here</span>);

  return (
    <Table striped size="sm">
      <tbody>
        {steps.list.map( (step, index) => {
          return (
            <tr key={ index }>
            
              <td align="center">
                <ButtonGroup size="sm">
                  <Button variant="secondary" onClick={ () => {
                    dispatch({
                      type: ACTIONS.MOVE_UP_STEP,
                      payload: { index, step }
                    })
                  }}>
                    <ArrowUp />
                  </Button>

                  <Button variant="secondary" onClick={ () => {
                    dispatch({
                      type: ACTIONS.MOVE_DOWN_STEP,
                      payload: { index, step }
                    })
                  }}>
                    <ArrowDown />
                  </Button>

                </ButtonGroup>
              </td>
              
              <td>{index+1}.</td>

              <td>{ step.title }</td>

              <td align="right">
                <ButtonGroup size="sm">
                  <Button 
                    onClick={ () => {
                      handleStepModalOpen();
                      dispatch({
                        type: ACTIONS.EDIT_STEP,
                        payload: { index, step }
                      })
                  }}>
                    Edit
                  </Button>
                </ButtonGroup>
              </td>

            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

const SubstepItem = ({index, substep, substeps, dispatch, setExistingStepAsUpdated}) => {
  const [editField, setEditField] = useState(false);
  const [editText, setEditText] = useState(substep);
  
  const editTextField = () => {
    return (
      <div className="form-floating mt-2 mb-2">
        <textarea className="form-control"
          id="floatingTextarea"
          style={{ height: "200px" }}
          placeholder="Recipe substep instructions" 
          value={ editText }
          onChange={ e => setEditText(e.target.value) }

        >
        </textarea>
        <label htmlFor="floatingTextarea">Editing Substep</label>
      </div>
    )
  };

  const EditButton = () => {
    return (
      <button 
        type="button" className="btn btn-primary btn-sm"
        onClick={ () => {
          setExistingStepAsUpdated();
          onEdit();
        }}
      >
        Edit 
      </button>
    );
  };

  const onEdit = () => {
    setEditField( () => !editField );
  }

  const editButtonGroup = () => {
    return (
      <div className="container mt-2 mb-2">
        <div className="row">

          <div className="col">

            <ButtonGroup size="sm">

              <EditButton />

              <Button variant="secondary" onClick={ () => {
                setExistingStepAsUpdated();
                dispatch({
                  type: ACTIONS.MOVE_UP_SUBSTEP,
                  payload: { index }
                })}}
              >
                <ArrowUp />
              </Button>

              <Button variant="secondary" onClick={ () => {
                setExistingStepAsUpdated();
                dispatch({
                  type: ACTIONS.MOVE_DOWN_SUBSTEP,
                  payload: { index }
                })}}
              >
                <ArrowDown />
              </Button>

            </ButtonGroup>
          </div>

          <div className="col">

            <ButtonGroup size="sm">
              <Button variant="danger" onClick={ () => {
                setExistingStepAsUpdated();
                dispatch({
                  type: ACTIONS.REMOVE_SUBSTEP,
                  payload: { index }
                })
              }}>
                Delete
              </Button>
            </ButtonGroup>

          </div>

        </div>
      </div>
    );

  }

  const substepEditResetButton = () => {
    return (
      <button type="button" className="btn btn-secondary btn-sm"
        onClick={ () => setEditText(() => substep )}
      > Reset
      </button>
    );
  }

  const substepEditCloseButton = () => {
    return (
      <button type="button" className="btn btn-secondary btn-sm"
        onClick={ () => {
          setEditText(() => substep );
          onEdit();
        }}
      > Close
      </button>
    );
  }

  const saveButton = () => {
    return (
      <button type="button" className="btn btn-primary btn-sm"
        onClick={ () => onSave() }
      > Save
      </button>
    );
  }

  const onSave = () => {
    dispatch({
      type: ACTIONS.SAVE_SUBSTEP,
      payload: { index, substep: editText }
    });
    onEdit()
  };

  const saveButtonGroup = () => {
    return (
      <div className="btn-group" role="group" aria-label="Save Button Group">
        { substepEditResetButton() }
        { substepEditCloseButton() }
        { saveButton() }
      </div>
    );
  }

  return (
    <li>
        { editField ? editTextField() : editText }
        { editField ? saveButtonGroup() : editButtonGroup() }
    </li>
  );
}

const SubstepList = ({ substep, substeps, dispatch, setExistingStepAsUpdated }) => {
  const Substeps = () => {
    return substeps.map( (substep, index) => {
      return (
        <SubstepItem 
          key={ index }
          index={ index }
          substep={ substep }
          substeps={ substeps }
          dispatch={ dispatch }
          setExistingStepAsUpdated={ setExistingStepAsUpdated }
        />
      );
    });
  };

  return (
    <ol> 
      { (substeps.length > 0 ) ? <Substeps /> : <span></span> }
    </ol>
  );

};

const StepModalContent = ({ steps, dispatch, setExistingStepAsUpdated }) => {
  const SECTION_KEY = 'steps';

  return (
    <>

      <div className="col-md-12">

        <label htmlFor="title" className="form-label">Step Title</label>

        <input type="text" className="form-control"
          id="title" name="title"
          value={ steps.title }
          onChange={ event => {
            setExistingStepAsUpdated();
            dispatch({ 
              type: ACTIONS.ON_CHANGE,
              payload: { key: SECTION_KEY, event }
            })}
          }
        />

      </div>
      
      <div className="col-md-12">
        <SubstepList
          substeps={ steps.substeps }
          dispatch={ dispatch }
          setExistingStepAsUpdated={ setExistingStepAsUpdated }
        />
      </div>

      <div className="col-md-12">

        <label htmlFor="substepText" className="form-label">New Substep Instructions:</label>

        <textarea className="form-control" id="substepText" rows="3"
          name="substepText"
          value={ steps.substepText }
          onChange={ event => {
            setExistingStepAsUpdated();
            dispatch({ 
              type: ACTIONS.ON_CHANGE,
              payload: { key: SECTION_KEY, event }
            })}
          }
        />
      </div>

      <div className="col-md-12 ">
        <button type="button"
          className="btn btn-primary btn-sm mt-md-3"
          onClick={ () => {
            setExistingStepAsUpdated();
            dispatch({
              type: ACTIONS.ADD_SUBSTEP,
              payload: { substepText: steps.substepText }
            })}
          }
        >
          <b>+</b> Add Substep
        </button>
      </div>

    </>
  );
};


const StepModal = (props) => {

  const { 
    index,
    steps,
    dispatch,
    stepModalFunctions,
    hasExistingStepUpdatedFunctions,
  } = props;

  const stepModal = {
    "title": "Recipe Step",
    "closeButtonText": "Close",
    "saveButtonText": "Save",
    "undoButtonText": "Undo All"
  }
  
  const initialStepValues = useRef([]);
  useEffect(() => {
    const stepIndex = steps.currentStepIndex;
    initialStepValues.current = steps.list[stepIndex];
  }, [steps.currentStepIndex])
  
  const getInitialSubsteps = () => {
    const initValues = initialStepValues.current;
    return initValues;
  }
  
  const {
    showStepModal,
    handleStepModalOpen,
    handleStepModalClose,
    handleStepModalSave,
  } = stepModalFunctions;

  const { 
    existingStepHasBeenUpdated,
    setExistingStepAsUpdated,
    resetExistingStepAsUpdated,
  } = hasExistingStepUpdatedFunctions;

  return (
    <>
      <div className="col-12">
        <Button variant="primary" onClick={handleStepModalOpen}>
          + Add Step
        </Button>
      </div>

      <Modal 
        show={showStepModal}
        onHide={handleStepModalClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{stepModal.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          
          <StepModalContent
            steps={ steps }
            dispatch={ dispatch }
            setExistingStepAsUpdated={ setExistingStepAsUpdated }
          />
          
          
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleStepModalClose}>
            { stepModal.closeButtonText }
          </Button>
          <Button variant="primary" onClick={handleStepModalSave}>
            { stepModal.saveButtonText }
          </Button>
        </Modal.Footer>
      </Modal>

    </>
  )

};


export const StepSection = ({ steps, dispatch }) => {

  const existingStepHasBeenUpdated = useRef(false);
  const setExistingStepAsUpdated = () => {
    existingStepHasBeenUpdated.current = true;
  }
  const resetExistingStepAsUpdated = () => {
    existingStepHasBeenUpdated.current = false;
  }
  const hasExistingStepUpdatedFunctions = {
    existingStepHasBeenUpdated,
    'setExistingStepAsUpdated': setExistingStepAsUpdated,
    'resetExistingStepAsUpdated': resetExistingStepAsUpdated,
  };

  const [showDiscardChangesModal, setShowDiscardChanges] = useState(false);

  const [showStepModal, setShowStepModal] = useState(false);

  const handleStepModalOpen = () => {
    setShowStepModal(true);
  };

  const handleStepModalClose = () => {
    if (!existingStepHasBeenUpdated.current) {
      setShowStepModal(false);
      return;
    }

    setShowDiscardChanges(true);
    return;
  };

  const handleStepModalSave = () => {
    resetExistingStepAsUpdated();
    dispatch({
      type: ACTIONS.SAVE_STEP,
      payload: { steps }
    });

    setShowStepModal(false);
  }

  const handleDiscardChangesDiscard = () => {
    setShowDiscardChanges(false);
    resetExistingStepAsUpdated();
    handleStepModalClose();
  };

  const handleDiscardChangesSave = () => {
    setShowDiscardChanges(false);
    handleStepModalSave();
  };

  const stepModalFunctions = {
    showStepModal,
    'handleStepModalOpen': handleStepModalOpen,
    'handleStepModalClose': handleStepModalClose,
    'handleStepModalSave': handleStepModalSave,
  };
  
  return (
    <>
      <h3 className="header">Edit Recipe Steps</h3>

      <StepList 
        handleStepModalOpen={ handleStepModalOpen }
        steps={ steps } 
        dispatch={ dispatch }
      />

      <StepModal
        stepModalFunctions={ stepModalFunctions }
        hasExistingStepUpdatedFunctions={ hasExistingStepUpdatedFunctions }
        steps={ steps }
        dispatch={ dispatch }
      />

      <DiscardChangesModal 
        showDiscardChangesModal={ showDiscardChangesModal }
        setShowDiscardChanges={ setShowDiscardChanges }
        handleStepModalSave={ handleStepModalSave }
        handleDiscardChangesDiscard={ handleDiscardChangesDiscard }
        handleDiscardChangesSave={ handleDiscardChangesSave }
      />

    </>  
  );
  }