import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

export const RecipeSteps = (props) => {
  let steps = props.steps;

  return (
    <Container>
      <Row>
        {steps.map((step, index) => {
          return (
            <Col xl={4} lg={6} key={index}>
              <Card className="mb-2">
                <Card.Body>
                  <Card.Title className="header">
                    {++index}. {step.title}
                  </Card.Title>
                  <RecipeCardSubSteps substeps={step.substeps} />
                </Card.Body>
              </Card>
            </Col>
          );
        })}
      </Row>
    </Container>
  );
};


const RecipeCardSubSteps = (props) => {
  return props.substeps.map((bullet, index) => {
    return (
      <Card.Text key={index}>
        {bullet}
      </Card.Text>
    );
  });
};