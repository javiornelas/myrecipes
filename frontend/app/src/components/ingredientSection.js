import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import { ArrowUp, ArrowDown, XLg } from 'react-bootstrap-icons';

import { ACTIONS } from './Forms/recipeFormReducer';

const gcd = (a, b) => {
  if (b < 0.0000001) return a;
  return gcd(b, Math.floor(a % b));
}

const getFraction = (decimal) => {
  decimal = parseFloat(decimal).toFixed(2);

  const len = decimal.toString().length - 2;
  let denominator = Math.pow(10, len);
  let numerator = decimal * denominator;
  
  let divisor = gcd(numerator, denominator);

  numerator /= divisor;
  denominator /= divisor;

  return `${numerator.toString()}/${denominator.toString()}`;
};

export const convertToFraction = (number) => {
  number = parseFloat(number);
  const isWholeNumber = (number % 1) === 0;
  if ( isWholeNumber ) return number;

  let wholeNumber = parseInt(number);
  let fraction = getFraction(number % 1);

  wholeNumber = ( wholeNumber === 0 ) ? '': `${wholeNumber}`

  const mixedNumber = `${wholeNumber} ${fraction}`
  return mixedNumber;
}


const IngredientsToAdd = ({ list, dispatch}) => {
  if ( list === undefined || list.length === 0 ) {
    return (
      <span>Add Ingredients in this Section</span>
    );
  }

  return (
    <Table striped size="sm">
      <tbody>

      {list.map( (ingredient, index) => {
        const { name, quantity, quantityType } = ingredient;

        return (
            <tr key={ index }>

              <td>
                <ButtonGroup size="sm">

                  <Button variant="secondary" onClick={ () => {
                      dispatch({
                        type: ACTIONS.MOVE_UP_INGREDIENT,
                        payload: { index, ingredient }
                      })
                    }}
                  >
                    <ArrowUp />
                  </Button>

                  <Button variant="secondary" onClick={ () => {
                      dispatch({
                        type: ACTIONS.MOVE_DOWN_INGREDIENT,
                        payload: { index, ingredient }
                      })
                    }}
                  >
                    <ArrowDown />
                  </Button>

                </ButtonGroup>

              </td>

              <td> 
                { convertToFraction(quantity) } { quantityType } { name }
              </td>

              <td align="right">
                <ButtonGroup>
                  <Button variant="danger" arial-label="close" onClick={ () => {
                      dispatch({
                        type: ACTIONS.REMOVE_INGREDIENT,
                        payload: { index, ingredient }
                      })
                    }}
                  >
                    <XLg />
                  </Button>
                </ButtonGroup>
              </td>

            </tr>
          );
        })}

      </tbody>
    </Table>
  );
};

export const IngredientSection = ( props ) => {

  const { ingredients, dispatch  } = props;
  const { name, quantity, quantityType } = ingredients;

  const SECTION_KEY = 'ingredients';

  return (
    <>
      { ( ingredients.length === 0 )
          ? <h3>Add Ingredients</h3>
          : <h3 className="header">Edit Ingredients</h3>
      }

      <IngredientsToAdd 
        list={ ingredients.list }
        dispatch={ dispatch }
      />

      <div className="col-md-8">
        <label htmlFor="ingredientName" className="form-label">
          Ingredient Name
        </label>

        <input type="text" className="form-control" id="ingredientName"
          name="name" value={name} required
          onChange={ event => dispatch({ 
            type: ACTIONS.ON_CHANGE,
            payload: { key: SECTION_KEY, event }
          })}
        />
      </div>

      <div className="col-6 col-md-2">
        <label htmlFor="ingredientQuantity" className="form-label">Quantity</label>
        <input type="number" className="form-control" id="ingredientQuantity"
        name="quantity" value={quantity} min="1"
        onChange={ event => dispatch({ 
          type: ACTIONS.ON_CHANGE,
          payload: { key: SECTION_KEY, event }
        })}
        />
      </div>

      <div className="col-6 col-md-2">
        <label htmlFor="inputState" className="form-label">Quantity Type</label>
        <select id="inputState" className="form-select"
        name="quantityType" value={quantityType}
        onChange={ event => dispatch({ 
          type: ACTIONS.ON_CHANGE,
          payload: { key: SECTION_KEY, event }
        })}
        >
        <option value=""></option>
        <option value="oz">OZ</option>
        <option value="tsp">TSP</option>
        <option value="tbsp">TBSP</option>
        <option value="cup">CUP</option>
        <option value="lb">LB</option>
        </select>     
      </div>

      <div className="col-12">
        <Button type="button" 
          onClick={ () => {
            dispatch({
              type: ACTIONS.ADD_INGREDIENT,
              payload: { name, quantity, quantityType }
            })
          }}
        >
        <b>+</b> Add Ingredient
        </Button>
      </div>
    </>  
  );
}