import HomeNav from './nav.js';
import { AuthProvider } from '../custom_hooks/useAuth';

export default function Header(props) {
    return (
      <AuthProvider>
        <HomeNav brand="My Simple Cookbook"/>
      </AuthProvider>
    )
  }
