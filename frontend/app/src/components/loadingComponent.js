import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Spinner from "react-bootstrap/Spinner";

export const LoadingScreen = () => {
  return (
    <Container fluid>
      <Row 
        className="justify-content-center align-items-center"
        style={{ height: "80vh" }}
      >
        <Col xs="auto">

          <Spinner animation="border" role="status" variant="primary">
            <span className="visually-hidden">Loading...</span>
          </Spinner>

        </Col>
      </Row>
    </Container>
  );
}