import Badge from "react-bootstrap/Badge";
import pizzaImage from "../data/pizza_background_small.jpg";
import { Link } from "react-router-dom";

import { LoadingScreen } from "./loadingComponent";

const RecipeCard = (props) => {
  let { img_link } = props.recipe;
  const { name, recipe_id } = props.recipe;
  const { prep_time, cook_time, calories } = props.recipe;
  const { is_public: isPublic, owner } = props.recipe;

  if (img_link === "" || img_link === "undefined") {
    img_link = pizzaImage;
  }

  const showBadge = (text) => {
    return (
      <>
        <Badge bg="secondary">{text}</Badge>{" "}
      </>
    );
  };

  return (
    <div className="card mb-2 flex-fill">
      <div>
        <img src={img_link} className="img-fluid rounded-start" alt="" />
      </div>

      <div>
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">
            <small className="text-muted">
              Prep {prep_time} mins | Cook {cook_time} mins | {calories}{" "}
              calories
            </small>
          </p>
          {owner === "true" ? showBadge("Own") : ""}
          {isPublic === 1 ? showBadge("Public") : ""}
          <Link className="stretched-link" to={`/recipe/${recipe_id}`}></Link>
        </div>
      </div>
    </div>
  );
};

const shouldRecipeShow = (filter, isPublic, owner) => {
  switch (filter) {
    case "ALL":
      return true;
    case "PUBLIC":
      if (isPublic === 1) return true;
      return false;
    case "OWN":
      if (owner === "true") return true;
      return false;
    default:
      return false;
  }
};

const recipeCard = (index, recipe, filter) => {
  const { is_public: isPublic, owner } = recipe;
  const shouldShow = shouldRecipeShow(filter, isPublic, owner);
  if (!shouldShow) return;

  return (
    <div className={`col-md-6 d-flex align-items-stretch`} key={index}>
      <RecipeCard recipe={recipe} />
    </div>
  );
};

const RecipeList = (props) => {
  const listRecipes = (recipes) => {
    if (recipes !== undefined && recipes.length > 0) {
      return recipes.map((recipe, index) => {
        return recipeCard(index, recipe, props.filter);
      });
    }

    return <LoadingScreen />;
    // return <h1>No recipes loaded</h1>;
  };

  return <div className="row">{listRecipes(props.list)}</div>;
};

export default RecipeList;
