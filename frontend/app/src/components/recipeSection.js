import React, { useEffect } from "react";

import { ACTIONS } from "./Forms/recipeFormReducer";

export const RecipeSection = ({ recipe, dispatch }) => {
  const SECTION_KEY = "recipe";

  return (
    <>
      <div className="col-md-12">
        <label htmlFor="recipeName" className="form-label">
          Recipe Name
        </label>
        <input
          type="text"
          className="form-control"
          id="name"
          name="name"
          value={recipe.name}
          onChange={(event) =>
            dispatch({
              type: ACTIONS.ON_CHANGE,
              payload: { key: SECTION_KEY, event },
            })
          }
          required
        />
      </div>

      <div className="col-6 col-md-3">
        <label htmlFor="servings" className="form-label">
          Serving Size
        </label>
        <input
          type="number"
          name="servings"
          className="form-control"
          id="servings"
          min="1"
          value={recipe.servings}
          onChange={(event) =>
            dispatch({
              type: ACTIONS.ON_CHANGE,
              payload: { key: SECTION_KEY, event },
            })
          }
        />
      </div>

      <div className="col-6 col-md-3">
        <label htmlFor="numberOfCalories" className="form-label">
          Calories
        </label>
        <input
          type="number"
          className="form-control"
          min="0"
          id="numberOfCalories"
          name="calories"
          value={recipe.calories}
          onChange={(event) =>
            dispatch({
              type: ACTIONS.ON_CHANGE,
              payload: { key: SECTION_KEY, event },
            })
          }
        />
      </div>

      <div className="col-6 col-md-3">
        <label htmlFor="prep_time" className="form-label">
          Prep Time
        </label>
        <input
          type="number"
          className="form-control"
          min="0"
          id="prep_time"
          placeholder=""
          name="prep_time"
          value={recipe.prep_time}
          onChange={(event) =>
            dispatch({
              type: ACTIONS.ON_CHANGE,
              payload: { key: SECTION_KEY, event },
            })
          }
        />
      </div>

      <div className="col-6 col-md-3">
        <label htmlFor="cook_time" className="form-label">
          Cook Time
        </label>
        <input
          type="number"
          className="form-control"
          min="0"
          id="cook_time"
          placeholder=""
          name="cook_time"
          value={recipe.cook_time}
          onChange={(event) =>
            dispatch({
              type: ACTIONS.ON_CHANGE,
              payload: { key: SECTION_KEY, event },
            })
          }
        />
      </div>

      <div className="col-md-3">
        <label htmlFor="isPublic" className="form-label">
          Public Recipe
        </label>
        <input
          type="checkbox"
          className="form-check-input ms-1"
          aria-label="Is recipe public?"
          name="isPublic"
          value="1"
          onChange={(event) =>
            dispatch({
              type: ACTIONS.ON_PUBLIC_CHANGE,
              payload: { checked: event.target.checked },
            })
          }
          checked={recipe.isPublic === 1 ? true : false}
        />
      </div>
    </>
  );
};
