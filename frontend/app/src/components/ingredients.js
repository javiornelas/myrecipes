import Table from 'react-bootstrap/Table';
import { convertToFraction } from "./ingredientSection";

const IngredientList = (props) => {
  let list = props.list;
  return (
    <Table striped size="sm">
      <thead>
        <tr>
          <td><h3>Ingredients</h3></td>
        </tr>
      </thead>
      <tbody>
        {list.map(({ quantity, quantityType, name }, index) => {
          return (
            <tr key={index}>
              <td key={index}>
                {convertToFraction(quantity)} {quantityType} {name}
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

export const Ingredients = (props) => {
  return (
    <div className="container-fluid">
      <IngredientList list={props.ingredients} />
    </div>
  );
};
