import { createContext, useCallback, useContext, useEffect, useState, useRef } from 'react';
import { useLocation, useNavigate, Navigate } from 'react-router-dom';

import { login, logout, authorized } from '../api/login';

import { LoadingScreen } from '../components/loadingComponent';

const authContext = createContext();

function useAuth() {
  const [authed, setAuthed] = useState(undefined);
  const isAuthorizing = useRef(false);
  const navigate = useNavigate();
  const { state } = useLocation();
  const location = useLocation();

  const getAuthorization = useCallback( async () => {
    isAuthorizing.current = true;
    const authResponse = await authorized();
    
    isAuthorizing.current = false;
    return authResponse;
  }, []);
  
  useEffect( () => {
    if (isAuthorizing.current === true) return;
    getAuthorization()
      .then(( authResponse ) => {
        setAuthed(authResponse);

        const pathAndAuthResponseBool = (
          (location.pathname === "/login" ) 
          && authResponse
        );

        if (pathAndAuthResponseBool) navigate( state?.path || "/recipes");
      })
      .catch(error => console.error(`Error in getAuthorization: ${error}`));
  }, []);
  // }, [getAuthorization, location.pathname, navigate, state?.path]);

  const userLogin = (username, password) => {
    return new Promise( async (res) => {
      const loginResponse = await login(username, password);

      if ( loginResponse === 200 ) {
        setAuthed( () => true );
        navigate( state?.path || "/recipes");
      }

      res(loginResponse);
    });
  };

  const userLogout = () => {
    return new Promise( async (res) => {
      const logoutResponse = await logout();
      if ( logoutResponse ) {
        setAuthed(() => false);
        res();
      }
    });    
  };

  return {
    authed,
    "login": userLogin,
    "logout": userLogout,
  };
}

export const AuthProvider = ({ children }) => {
  const auth = useAuth();

  return <authContext.Provider value={auth}> { children } </authContext.Provider>;
};

export const RequireAuth = ({ children }) => {
  const { authed } = useAuth('');
  const location = useLocation();

  const waitingElement = <h1>Waiting for response</h1>

  const redirectToLogin = <Navigate to="/" replace state={{ path: location.pathname }}/>

  const AuthRedirect = () => {
    if ( authed === '' ) return waitingElement;
    if ( authed === true ) return children;
    if ( authed === false ) return redirectToLogin;
  };
  
  return <AuthRedirect />;
};

export const LoginCheck = ({children}) => {
  return (
    <AuthProvider>
      <LoginAuthCheck>
        {children}
      </LoginAuthCheck>
    </AuthProvider>
  )
}

const LoginAuthCheck = ({children}) => {
  const { authed } = useAuth();

  const redirectToLogin = <Navigate to="/recipes" replace state={{ path: '/' }}/>

  if (authed) return redirectToLogin;
  if (authed === undefined) return <LoadingScreen />;

  return children;
};

export default function AuthConsumer() {
  return useContext(authContext);
};