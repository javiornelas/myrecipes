import { API_URL } from "./settings";

export const getProfile = async () => {
  try {
    const response = await fetch(API_URL.user, FETCH_GET_OBJ);
    const user = response.json();
    return user;
  }
  catch (error) {
    console.error(`We have an error ${error}`);
  }

};

// const FETCH_POST_OBJ = {
//   mode: "cors",
//   credentials: "include",
//   headers: {
//     "Accept" : "*/*",
//     "Content-Type": "application/json",
//   },
// }

const FETCH_GET_OBJ = {
  method: "GET",
  mode: "cors",
  credentials: "include",
  headers: {
    "Accept" : "*/*",
    "Content-Type": "application/json",
}};