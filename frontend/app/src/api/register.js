import { API_URL, registerMessages } from './settings';

export const register = async ( username, password ) => {
  try {
    const response = fetch(API_URL.register, {
      method: "POST",
      mode: 'cors',
      credentials: 'include',
      headers: {
        "Accept" : "*/*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({username, password})
    }).then( response => response.json() );

    return response;

  } catch (error) {
    console.error(`We have an error: ${error}`);
  }  
};

export const getStatusMessage = (status) => {
  if ( status === 'passwordMismatch') return registerMessages.passwordMismatch;
  if ( status >= 500) return registerMessages.serverError;
  if ( status >= 400 ) return registerMessages.invalid;
  if ( status >= 200 ) return registerMessages.success;
};