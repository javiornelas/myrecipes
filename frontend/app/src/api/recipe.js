import { API_URL } from './settings';

export const create = async (recipe) => {
  try {
    const response = fetch(API_URL.createRecipe, {
      ...FETCH_POST_OBJ,
      body: JSON.stringify(recipe)
    })
    .then( response => response.json())
    .then( ({status, message, id}) => {
      return { status, message, id }
    })

    return response;

  } catch (error) {
    console.error(`We have an error: ${error}`);
  }
};

export const edit = async (recipe) => {

  const updateRecipeUrl = `${API_URL.updateRecipe + recipe.id}`

  if ( recipe.id ) delete recipe.id;

  try {
    const response = fetch(updateRecipeUrl,{
      ...FETCH_POST_OBJ,
      body: JSON.stringify(recipe)
    })
    .then( response => response.json())
    .then( data => data );

    return response;

  } catch (error) {
    console.error(`We have an error: ${error}`);
  }
};

export const getAll = async () => {
    try {
      const response = await fetch(API_URL.allRecipes, FETCH_GET_OBJ)
      
      const data = response.json();

      return data;
      
    } catch (error) {
      console.error(`We have an error: ${error}`);
    }
};

export const getRecipe = async (recipeID) => {
  try {
    const recipeUrl = `${API_URL.recipeById}${recipeID}`;

    const response = await fetch(recipeUrl, FETCH_GET_OBJ);
    
    const data = response.json();
    return data;
    
  } catch (error) {
    console.error(`We have an error: ${error}`);
  }
};

export const deleteRecipe = async (recipeID) => {
  try {
    const recipeUrl = `${API_URL.deleteRecipe}${recipeID}`;

    const response = await fetch(recipeUrl, FETCH_DEL_OBJ);
    
    const data = response.json();

    return data;

  } catch (error) {
    console.error(`We have an error: ${error}`)
  }
};

const FETCH_POST_OBJ = {
  method: "POST",
  mode: "cors",
  credentials: "include",
  headers: {
    "Accept" : "*/*",
    "Content-Type": "application/json",
  },
}

const FETCH_GET_OBJ = {
  method: "GET",
  mode: "cors",
  credentials: "include",
  headers: {
    "Accept" : "*/*",
    "Content-Type": "application/json",
}};

const FETCH_DEL_OBJ = {
  method: "DELETE",
  mode: "cors",
  credentials: "include",
  headers: {
    "Accept" : "*/*",
    "Content-Type": "application/json",
}};