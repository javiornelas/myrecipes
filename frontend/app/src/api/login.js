import { API_URL, loginMessages } from './settings';

export const login = async (username, password) => {

  try {
    const status = fetch(API_URL.login, {
      method: "POST",
      mode: 'cors',
      credentials: 'include',
      headers: {
        "Accept" : "*/*",
        "Content-Type": "application/json",
      },
      agent: null,
      body: JSON.stringify({username, password})
    }).then( response => response.status );

    return status;

  } catch (error) {
    console.error(`We have an error: ${error}`);
  }

};

export const logout = () => {
  return new Promise( async(resolve, reject) => {

    fetch(API_URL.logout, {
      method: "GET",
      mode: 'cors',
      credentials: 'include',
      headers: {
      "Content-Type": "application/json",
    }
    }).then( response => {
      if ( response.status === 200 ) resolve(true);
      resolve(false);
    });
  });
};


export const authorized = async () => {
  return new Promise( async(resolve, reject) => {

    fetch(API_URL.authorize, {
      method: "GET",
      credentials: 'include',
      mode: 'cors',
      headers: {
        "Accept" : "*/*",
        "Content-Type": "application/json",
    }
    }).then( response => {
      if ( response.status === 200 ) resolve(true);
      resolve(false);
    }).catch((error) => {
      console.error(`@authorized function: ${error}`);
    });
  });
};

export const getStatusMessage = (status) => {
  if ( status >= 500) return loginMessages.serverError;
  if ( status >= 400 ) return loginMessages.invalid;
  if ( status >= 200 ) return loginMessages.success;
};
