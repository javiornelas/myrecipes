const isIPAddress = (ip) => {
    const ipAddressRegex = /^(\d{1,3}\.){3}\d{1,3}$/;
    return ipAddressRegex.test(ip);
}

const SET_API_URL = () => {

    let server = null;
    if (isIPAddress(process.env.REACT_APP_BACKEND_HOST) 
        || (process.env.REACT_APP_USE_BACKEND_PORT === "true")
        ) { 
        server = `${process.env.REACT_APP_BACKEND_HOST}:${process.env.REACT_APP_BACKEND_PORT}`;
    } else {
        server = `${process.env.REACT_APP_BACKEND_HOST}`;
    }

    let proto = null;
    if (process.env.REACT_APP_USE_HTTPS === "true") { proto = 'https://'; }
    else { proto = 'http://'; }


    server = `${proto}${server}`;
    
    console.log(`Backend Server: ${server}`);

    return server;
}

const API_URL = SET_API_URL();

exports.API_URL = {
    "user": `${API_URL}/user`,
    "login": `${API_URL}/login`,
    "logout": `${API_URL}/logout`,
    "authorize": `${API_URL}/authorize`,
    "register": `${API_URL}/register`,

    "allRecipes": `${API_URL}/recipe`,
    "recipeById": `${API_URL}/recipe/`,
    "createRecipe": `${API_URL}/recipe/create`,
    "updateRecipe": `${API_URL}/recipe/update/`,
    "deleteRecipe": `${API_URL}/recipe/delete/`,
};

exports.loginMessages = { 
    "success": "Login Success!",
    "invalid": "Invalid Username or Password",
    "serverError": "Oops something went wrong",
};

exports.registerMessages = {
    "success": "User has been registered",
    "passwordMismatch": "Passwords do not match",
    "invalid": "Invalid registration Username or Password",
    "serverError": "Oops something went wrong",
};
