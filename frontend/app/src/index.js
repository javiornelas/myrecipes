import React from 'react';
import ReactDOM from 'react-dom/client';
import './css/index.css';
import './css/main.css';
// import reportWebVitals from './reportWebVitals';

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import App from './App';

import { LoginCheck, RequireAuth } from './custom_hooks/useAuth';

import LoginPage from './views/LoginPage';

import RecipePage from './views/RecipePage';
import RecipeFormPage from './views/RecipeFormPage';
import RemoveRecipePage from './views/RemoveRecipePage';
import ProfilePage from './views/Profile';
import SettingsPage from './views/Settings';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
      {/* <AuthProvider> */}
        <BrowserRouter>
          <Routes>
            <Route path="/" 
              element={ <LoginCheck> <LoginPage /> </LoginCheck> }
            />
            <Route path="/recipes"
              element={ <RequireAuth> <App /> </RequireAuth>  }
            />
            <Route path="/recipe/:recipeId"
              element={ <RequireAuth> <RecipePage /> </RequireAuth> } 
            />
            <Route path="/recipe/:recipeId/edit"
              element={ <RequireAuth> <RecipeFormPage /> </RequireAuth> } 
            />
            <Route path="/recipe/add" 
              element={ <RequireAuth> <RecipeFormPage /> </RequireAuth> }
            />
            <Route path="/remove_recipe" 
              element={ <RequireAuth> <RemoveRecipePage /> </RequireAuth> }
            />
            <Route path="/profile" 
              element={ <RequireAuth> <ProfilePage /> </RequireAuth> }
            />
            <Route path="/settings" 
              element={ <RequireAuth> <SettingsPage /> </RequireAuth> }
            />
          </Routes>
        </BrowserRouter>
      {/* </AuthProvider> */}
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
