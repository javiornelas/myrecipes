const ingredients = [
  { "id": "0", "name": "Yellow Onion", "quantity": "1", "quantityType": ""},
  { "id": "1", "name": "Poblano Pepper", "quantity": "1", "quantityType": ""},
  { "id": "2", "name": "Lime", "quantity": "1", "quantityType": ""},
  { "id": "3", "name": "Cilantro", "quantity": "1", "quantityType": "oz"},
  { "id": "4", "name": "Southwest Spice Blend", "quantity": "1", "quantityType": "tbsp"},
  { "id": "5", "name": "Ground Turkey", "quantity": "1", "quantityType": "oz"},
  { "id": "6", "name": "Tex-Mex Paste", "quantity": "1", "quantityType": ""},
  { "id": "7", "name": "Cauliflower Rice", "quantity": "1", "quantityType": "oz"},
  { "id": "8", "name": "Smoky Red Pepper Crema", "quantity": "1", "quantityType": "tbsp"},
 ]

const steps = [
  { 
    "title": "Prep",
    "substeps": [
      "Wash and dry produce. Halve, peel, and thinly slice onion. Halve, deseed, and thinly slice poblano into strips. Zest and quarter lime. Pick cilantro leaves from stems; roughly chop leaves.",
    ],
  },
  { 
    "title": "Cook Veggies",
    "substeps": [
      "Heat a drizzle of oil in a large pan over medium-high heat. Add onion and poblano; season with half the Southwest Spice, salt, and pepper. Cook, stirring occasionally, until browned and softened, 7-9 minutes.",
      "Transfer to a medium bowl; cover to keep warm. Wipe out pan.",
    ],
  },
  { 
    "title": "Start Turkey",
    "substeps": [
      "Heat a drizzle of oil in pan used for veggies over medium-high heat. Add turkey; using a spatula press into an even layer. Cook, without stirring, until browned bottom , 3-4 minutes.",
      "Transfer to a medium bowl; cover to keep warm. Wipe out pan.",
    ],
  },
  { 
    "title": "Finish Turkey",
    "substeps": [
      "Once turkey is browned on bottom, break up meat into pieces. Stir in Tex-Mex paste, remaining Southwest Spice, and 1/4 cup water. Bring to a simmer and cook until sauce has thickened and turkey is cooked through, 2-3 minutes more.",
      "Remove from heat; stir in 1 TBSP butter until melted. Season with salt and pepper to taste.",
    ],
  },
  { 
    "title": "Cook Cauliflower Rice",
    "substeps": [
      "Heat a drizzle of oil in a medium pan over medium heat. Add cauliflower rice; season with 1/4 tsp salt and a pinch of pepper. Cook , stirring ocasionally, until cauliflower is slightly softened, 4-5 minutes.",
      "Remove from heat. Stir in lime zest, half the cilantro, 1 TBSP butter, and a squeeze of lime juice.",
    ],
  },
  { 
    "title": "Serve",
    "substeps": [
      "Divide cauliflower rice between bowls. Top with turkey and veggies. Drizzle with smoky red pepper crema. Garnish with remaining cilantro. Serve with remaining lime wedges on the side.",
    ],
  },
]

const recipeList = [
  {
    "name": "Southwest Turkey & Cauliflower Rice",
    "prep_time": "5",
    "cook_time": "30",
    "calories": "580",
    "servings": "2",
    "img_link": "https://picsum.photos/id/237/200",
    "img": require("./stock_clip.jpeg"),
    "id": "100",
    "ingredients": ingredients,
    "steps": steps,
  },
  {
    "name": "Southwest Turkey & Cauliflower Rice",
    "prep_time": "5",
    "cook_time": "30",
    "calories": "210",
    "servings": "2",
    "img_link": "https://picsum.photos/id/235/200",
    "id": "200",
    "ingredients": ingredients,
    "steps": steps,
  },
  {
    "name": "Southwest Turkey & Cauliflower Rice",
    "prep_time": "5",
    "cook_time": "30",
    "calories": "210",
    "servings": "2",
    "img_link": "https://picsum.photos/id/237/200",
    "id": "300",
    "ingredients": ingredients,
    "steps": steps,
  },
  {
    "name": "Southwest Turkey & Cauliflower Rice",
    "prep_time": "5",
    "cook_time": "30",
    "calories": "210",
    "servings": "2",
    "img_link": "https://picsum.photos/id/235/200",
    "id": "400",
    "ingredients": ingredients,
    "steps": steps,
  },
];

export function getRecipeSteps(id) {
  return getRecipe(id).steps;
};

export function getRecipe(id) {
  return recipeList.find( recipe => recipe.id === id );
};

export default function getRecipes() {
  return recipeList;
};
